Discover hosts using ICMP
=============================

|pipeline| |coverage|

.. |pipeline| image:: https://gitlab.com/blueskyjunkie/host_discover/badges/master/pipeline.svg
   :target: https://gitlab.com/blueskyjunkie/host_discover/commits/master
   :alt: pipeline status

.. |coverage| image:: https://gitlab.com/blueskyjunkie/host_discover/badges/master/coverage.svg
   :target: https://gitlab.com/blueskyjunkie/host_discover/commits/master
   :alt: coverage report


A command line utility for downloading standards documents from the 3GPP download site.

.. contents::

.. section-numbering::


Installation
------------

This utility is not distributed in pypi yet. Use flit to directly install the project into your
Python virtual environment.

.. code-block:: bash

   # flit installation
   source ~/venv/bin/activate
   pip install flit
   flit install -s
   which discover_hosts


Getting started
---------------

.. code-block:: bash

   > discover_hosts --help
   usage: discover_hosts [-h] [--timeout TIMEOUT] network

   Discover hosts on a network using ICMP

   positional arguments:
     network            IPv4 or IPv6 network in CIDR format to analyze.

   optional arguments:
     -h, --help         show this help message and exit
     --timeout TIMEOUT  Explorer process timeout in seconds, default 30

Since ICMP runs on a protected port, you must run this utility as root, preferably using sudo

.. code-block:: bash

   > sudo discover_hosts 192.168.0.0/23
   192.168.0.1: True
   .
   .
   .
   192.168.1.254: False
