#
#  Copyright (c) 2020 Russell Smiley
#
#  This file is part of host_discover.
#
#  You should have received a copy of the MIT License along with host_discover.
#  If not, see <https://opensource.org/licenses/MIT>.
#

import argparse
import ipaddress
import typing

IpAddress = typing.Union[ipaddress.IPv4Address, ipaddress.IPv6Address]
HostList = typing.List[IpAddress]


class UserOptions:
    """
    Represent user options parsed from command line arguments.
    """

    DEFAULT_TIMEOUT = 30

    def __init__(self):
        self.__parsed_arguments = None

        self.__parser = argparse.ArgumentParser(
            description="Discover hosts on a network using ICMP"
        )

        self.__parser.add_argument(
            "network", type=str, help="IPv4 or IPv6 network in CIDR format to analyze.",
        )

        self.__parser.add_argument(
            "--timeout",
            default=self.DEFAULT_TIMEOUT,
            type=int,
            help="Explorer process timeout in seconds, default {0}".format(
                self.DEFAULT_TIMEOUT
            ),
        )

    @property
    def hosts(self) -> HostList:
        return list(self.network.hosts())

    @property
    def network(self) -> typing.Union[ipaddress.IPv4Network, ipaddress.IPv6Network]:
        return ipaddress.ip_network(self.__parsed_arguments.network)

    @property
    def timeout(self) -> int:
        return self.__parsed_arguments.timeout

    def parse_arguments(self, command_line_arguments: typing.List[str]):
        self.__parsed_arguments = self.__parser.parse_args(command_line_arguments)
