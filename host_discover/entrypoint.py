#
#  Copyright (c) 2020 Russell Smiley
#
#  This file is part of host_discover.
#
#  You should have received a copy of the MIT License along with host_discover.
#  If not, see <https://opensource.org/licenses/MIT>.
#

import logging
import sys
import typing

from .discover import NetworkExplorer
from .options import UserOptions

log = logging.getLogger(__name__)

# TODO: user defined ping number of tests?
# TODO: user defined ping timeout?
# TODO: improve granularity of ping pass/fail feedback?
# TODO: add process timeout to feedback?
# TODO: what does reporting look like?


def main(command_line_arguments: typing.List[str]):
    try:
        user_options = UserOptions()
        user_options.parse_arguments(command_line_arguments)

        explorer = NetworkExplorer(user_options)
        exploration_results = explorer.explore()

        for this_result in exploration_results:
            print("{0}: {1}".format(this_result[0], this_result[1]))

    except KeyboardInterrupt:
        log.warning("Aborting program due to keyboard interrupt.")


def flit_entry():
    main(sys.argv[1:])
