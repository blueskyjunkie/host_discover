#
#  Copyright (c) 2020 Russell Smiley
#
#  This file is part of host_discover.
#
#  You should have received a copy of the MIT License along with host_discover.
#  If not, see <https://opensource.org/licenses/MIT>.
#

import typing
from concurrent.futures import ThreadPoolExecutor

import ping3

from host_discover.options import IpAddress, UserOptions


def ping_host(ip_address: IpAddress) -> bool:
    """
    Ping a host.

    :param ip_address:

    :return: True if host found.
    """
    result = ping3.ping(str(ip_address))
    if not result:
        value = False
    else:
        value = True

    return value


class NetworkExplorer:
    def __init__(self, user_options: UserOptions):
        self.__options = user_options

    def explore(self) -> typing.Iterator[typing.Tuple[IpAddress, bool]]:
        with ThreadPoolExecutor() as executor:
            futures = executor.map(
                ping_host, self.__options.hosts, timeout=self.__options.timeout
            )

        return zip(self.__options.hosts, futures)
