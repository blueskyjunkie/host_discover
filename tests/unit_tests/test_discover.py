#
#  Copyright (c) 2020 Russell Smiley
#
#  This file is part of host_discover.
#
#  You should have received a copy of the MIT License along with host_discover.
#  If not, see <https://opensource.org/licenses/MIT>.
#

import typing
from ipaddress import IPv4Address

# DO NOT DELETE! Used by mocker.patch below
import host_discover.discover
from host_discover.discover import NetworkExplorer, ping_host
from host_discover.options import HostList, IpAddress, UserOptions


class TestPingHosts:
    def test_host_exists(self, mocker):
        mock_ping_result = 0.26
        mocker.patch("host_discover.discover.ping3.ping", return_value=mock_ping_result)

        host_ip = IPv4Address("192.168.0.1")
        actual_result = ping_host(host_ip)

        assert actual_result == True

    def test_host_unknown(self, mocker):
        mock_ping_result = False
        mocker.patch("host_discover.discover.ping3.ping", return_value=mock_ping_result)

        host_ip = IPv4Address("192.168.0.1")
        actual_result = ping_host(host_ip)

        assert actual_result == False

    def test_host_timeout(self, mocker):
        mock_ping_result = None
        mocker.patch("host_discover.discover.ping3.ping", return_value=mock_ping_result)

        host_ip = IPv4Address("192.168.0.1")
        actual_result = ping_host(host_ip)

        assert actual_result == False


def fail_all_hosts(host: IpAddress) -> typing.List[typing.Tuple[IpAddress, bool]]:
    return False


def pass_all_hosts(host: IpAddress) -> typing.List[typing.Tuple[IpAddress, bool]]:
    return True


class TestNetworkExplorer:
    def test_all_failed_hosts(self, mocker):
        mock_config = UserOptions()
        mock_config.parse_arguments(["192.168.0.0/30"])

        mocker.patch("host_discover.discover.ping_host", side_effect=fail_all_hosts)
        under_test = NetworkExplorer(mock_config)
        actual_results = under_test.explore()

        expected_results = [
            (IPv4Address("192.168.0.1"), False),
            (IPv4Address("192.168.0.2"), False),
        ]
        assert all([x in expected_results for x in actual_results])

    def test_all_passed_hosts(self, mocker):
        mock_config = UserOptions()
        mock_config.parse_arguments(["192.168.0.0/30"])

        mocker.patch("host_discover.discover.ping_host", side_effect=pass_all_hosts)
        under_test = NetworkExplorer(mock_config)
        actual_results = under_test.explore()
        expected_results = [
            (IPv4Address("192.168.0.1"), True),
            (IPv4Address("192.168.0.2"), True),
        ]
        assert all([x in expected_results for x in actual_results])
