#
#  Copyright (c) 2020 Russell Smiley
#
#  This file is part of host_discover.
#
#  You should have received a copy of the MIT License along with host_discover.
#  If not, see <https://opensource.org/licenses/MIT>.
#

import pytest_mock

# DO NOT DELETE! Used by mocker.patch below
import host_discover.entrypoint
from host_discover.entrypoint import main


class TestMain:
    def test_execution(self, mocker):
        arguments = ["--network", "192.168.0.0/24"]

        mock_explorer = mocker.patch("host_discover.entrypoint.NetworkExplorer")
        mock_user_options = mocker.patch("host_discover.entrypoint.UserOptions")

        main(arguments)

        mock_options_instance = mock_user_options.return_value
        mock_options_instance.parse_arguments.assert_called_once_with(arguments)

        mock_explorer.assert_called_once_with(mock_user_options.return_value)
