#
#  Copyright (c) 2020 Russell Smiley
#
#  This file is part of host_discover.
#
#  You should have received a copy of the MIT License along with host_discover.
#  If not, see <https://opensource.org/licenses/MIT>.
#

import ipaddress

from host_discover.options import UserOptions


class TestUserOptions:
    def test_ipv4_network(self):
        arguments = [
            "192.168.0.0/30",
        ]

        under_test = UserOptions()
        under_test.parse_arguments(arguments)

        assert under_test.network == ipaddress.IPv4Network(arguments[0])

    def test_ipv6_network(self):
        arguments = [
            "feed:beef::/64",
        ]

        under_test = UserOptions()
        under_test.parse_arguments(arguments)

        assert under_test.network == ipaddress.IPv6Network(arguments[0])

    def test_ipv4_hosts(self):
        arguments = [
            "192.168.0.0/30",
        ]

        under_test = UserOptions()
        under_test.parse_arguments(arguments)

        assert under_test.hosts == [
            ipaddress.IPv4Address(x) for x in ["192.168.0.1", "192.168.0.2",]
        ]

    def test_ipv6_hosts(self):
        arguments = [
            "feed:beef::/126",
        ]

        under_test = UserOptions()
        under_test.parse_arguments(arguments)

        assert under_test.hosts == [
            ipaddress.IPv6Address(x)
            for x in ["feed:beef::1", "feed:beef::2", "feed:beef::3",]
        ]


class TestTimeoutOption:
    def test_default(self):
        arguments = [
            "feed:beef::/126",
        ]

        under_test = UserOptions()
        under_test.parse_arguments(arguments)

        assert under_test.timeout == 30

    def test_assign(self):
        arguments = ["feed:beef::/126", "--timeout", "10"]

        under_test = UserOptions()
        under_test.parse_arguments(arguments)

        assert under_test.timeout == 10
