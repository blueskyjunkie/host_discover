#
#  Copyright (c) 2020 Russell Smiley
#
#  This file is part of host_discover.
#
#  You should have received a copy of the MIT License along with host_discover.
#  If not, see <https://opensource.org/licenses/MIT>.
#

import re
import sys


def main():
    coverage_threshold = int(sys.argv[1])

    current_coverage = 0
    lines = sys.stdin.readlines()
    for this_line in lines:
        match_result = re.search(
            r"^.*((TOTAL)|((T|t)otal)).*\s+(\d{1,3})%.*$", this_line
        )
        if match_result is not None:
            current_coverage = int(match_result.group(5))

    if current_coverage < coverage_threshold:
        sys.exit(
            "FAILED: coverage threshold {0} (current) < {1} (threshold)".format(
                current_coverage, coverage_threshold
            )
        )


if __name__ == "__main__":
    main()
