#
#  Copyright (c) 2020 Russell Smiley
#
#  This file is part of host_discover.
#
#  You should have received a copy of the MIT License along with host_discover.
#  If not, see <https://opensource.org/licenses/MIT>.
#

import re
import sys

SEMANTIC_PATTERN = r"\d+\.\d+\.\d+"
SEMANTIC_RELEASE_PATTERN = r"^{0}$".format(SEMANTIC_PATTERN)
DRYRUN_SUFFIX_PATTERN = r"^({0})-dryrun\d*$".format(SEMANTIC_PATTERN)


def main():
    pipeline_id = sys.argv[1]
    ref_name = sys.argv[2]

    dryrun_result = re.search(DRYRUN_SUFFIX_PATTERN, ref_name)
    if re.search(SEMANTIC_RELEASE_PATTERN, ref_name) is not None:
        # Semantic release tag, so just propagate the semantic release
        print("{0}".format(ref_name))
    elif dryrun_result is not None:
        # Release dry run tag, so propagate the semantic version with the pipeline id
        print("{0}.{1}".format(dryrun_result.group(1), pipeline_id))
    else:
        # Just use the pipeline id
        print("0.0.0.{0}".format(pipeline_id))


if __name__ == "__main__":
    main()
